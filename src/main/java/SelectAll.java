import com.hibernate.models.Article;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class SelectAll {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();

        Transaction tx = session.beginTransaction();

        List<Article> articles = session.createQuery("FROM Article ").getResultList();

        for (Article article:articles){
            System.out.println(article);
        }

        session.close();
        sf.close();
    }
}
