import com.hibernate.models.Category;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Scanner;

public class ExerciceCategory {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();

        System.out.println("Que souhaitez vous faire ?");
        System.out.println("1 - Créer une catégorie");
        System.out.println("2 - Supprimer une catégorie");
        System.out.println("3 - Afficher toutes les catégories");
        System.out.println("4 - Editer une categorie");

        Scanner scanner = new Scanner(System.in);
        Integer userChoice = Integer.parseInt(scanner.nextLine());

        if (userChoice.equals(1)){
            System.out.println("Veuillez indiquer le libelle de la catégorie");
            String libelleCateg = scanner.nextLine();

            Category category = new Category();
            category.setLibelle(libelleCateg);

            Session session = sf.getCurrentSession();
            Transaction tx = session.beginTransaction();

            session.persist(category);

            tx.commit();

            session.close();
        } else if (userChoice.equals(2)){
            displayAllCategs(sf);
            System.out.println("Quelle catégorie voulez vous supprimer (saisir l'id)?");

            long id = Long.parseLong(scanner.nextLine());
            Session session = sf.getCurrentSession();
            Transaction tx = session.beginTransaction();

            session.remove(session.load(Category.class, id));

            tx.commit();
            session.close();
        } else if (userChoice.equals(3)){
            displayAllCategs(sf);
        } else if (userChoice.equals(4)){
            displayAllCategs(sf);
            System.out.println("Quelle catégorie voulez vous éditer ?");
            long idToEdit = Long.parseLong(scanner.nextLine());
            Session session = sf.getCurrentSession();
            Transaction tx = session.beginTransaction();

            Category categoryToEdit = session.load(Category.class, idToEdit);
            System.out.println("Saisir le nouveau libelle");
            String libelle = scanner.nextLine();
            categoryToEdit.setLibelle(libelle);

            tx.commit();
            session.close();
        }
    }

    public static void displayAllCategs(SessionFactory sessionFactory){
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        ArrayList<Category> categs = (ArrayList<Category>) session.createQuery("FROM Category ").getResultList();

        for (Category categ:categs){
            System.out.println(categ);
        }

        tx.commit();
        session.close();

    }
}
