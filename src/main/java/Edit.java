import com.hibernate.models.Article;
import com.hibernate.models.Category;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Edit {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Category category = session.load(Category.class, (long)3);
        category.setLibelle("Toto");

        tx.commit();
        session.close();
        sf.close();
    }
}
