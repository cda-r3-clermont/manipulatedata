import com.hibernate.models.Article;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Scanner;

public class SelectOne {
    public static void main(String[] args){
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        session.beginTransaction();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Quel id veut tu voir ?");

        long id = scanner.nextLong();

        Article article = session.load(Article.class, id);

        System.out.println(article);

        session.close();
        sf.close();
    }
}
