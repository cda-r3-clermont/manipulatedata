import com.hibernate.models.Address;
import com.hibernate.models.User;
import com.hibernate.utils.HibernateUtil;
import com.mysql.cj.xdevapi.AddResult;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Set;

public class HibernateValidation {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new HibernateUtil().buildSessionFactory();
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();

        User user = new User();
        user.setPassword("Aurelien63130");
        user.setAcceptTerm(true);
        user.setEmail("aureliendelorme1@gmail.com");
        user.setUsername("aur");

        Address address = new Address();
        address.setVille("Clermont-Ferrand");
        address.setPrenom("Aurélien");
        address.setNom("Delorme");
        address.setCodePostal("63190");

        user.setAdress(address);

        Set<ConstraintViolation<User>> errors = validator.validate(user);

        if(errors.isEmpty()){
            Session session = sessionFactory.getCurrentSession();
            Transaction tx = session.beginTransaction();
            session.persist(address);
            session.persist(user);
            tx.commit();
            session.close();
            System.out.println("BRAVO !!! Utilisateur enregistré !");
        } else {
            for (ConstraintViolation<User> error:errors){
                System.out.println(error.getMessage());
            }
        }


        sessionFactory.close();
    }
}
