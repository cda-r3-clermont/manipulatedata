import com.hibernate.models.Article;
import com.hibernate.models.Category;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Save {
    public static void main(String[] args){
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();

        Transaction tx = session.beginTransaction();

        Category category = new Category();
        category.setLibelle("Back-End");

        session.persist(category);

        Article article = new Article();
        article.setTitre("Hibernate");
        article.setCategory(category);
        article
                .setDescription("Trop bien Hibernate !");

        session.persist(article);

        tx.commit();

        session.close();

        sf.close();
    }
}
