package com.hibernate.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un nom")
    private String nom;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un prénom")
    private String prenom;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un code postal")
    @Size(min = 5, max = 6, message = "Le code postal n'est pas valide")
    private String codePostal;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir une ville")
    private String ville;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
