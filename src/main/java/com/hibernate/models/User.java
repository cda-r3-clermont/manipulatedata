package com.hibernate.models;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "utilisateur")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un username")
    @Length(min = 4, message = "Le username doit faire au moins 4 caractères")
    @Length(max = 20, message = "Le username ne peut pas faire plus de 20 caractères")
    private String username;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un email")
    @Email(message = "L'email n'est pas valide")
    private String email;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un mot de passe")
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
            message = "Le mot de passe doit avoir 8 caractères, au moins une lettre et un chiffre")
    private String password;

    @Temporal(TemporalType.DATE)
    @Past(message = "Vous ne pouvez pas être né dans le future")
    private Date dateNaissance;

    @Basic
    @AssertTrue(message = "Veuillez accepter les termes du contrat")
    private boolean acceptTerm;

    @OneToOne
    @Valid
    private Address adress;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public boolean isAcceptTerm() {
        return acceptTerm;
    }

    public void setAcceptTerm(boolean acceptTerm) {
        this.acceptTerm = acceptTerm;
    }

    public Address getAdress() {
        return adress;
    }

    public void setAdress(Address adress) {
        this.adress = adress;
    }
}
