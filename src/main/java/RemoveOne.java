import com.hibernate.models.Article;
import com.hibernate.models.Category;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Scanner;

public class RemoveOne {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Quel id veut tu supprimer ?");

        long id = scanner.nextLong();

        Category category = session.load(Category.class, id);
        session.remove(category);

        tx.commit();

        session.close();
        sf.close();
    }
}
